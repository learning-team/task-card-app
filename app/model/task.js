"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Task = (function () {
    function Task(content, completed, archived) {
        this.content = content;
        this.completed = completed;
        this.archived = archived;
    }
    return Task;
}());
exports.Task = Task;
//# sourceMappingURL=task.js.map